package br.com.caelum.financas.controller;

import br.com.caelum.financas.dao.ContaDao;
import br.com.caelum.financas.modelo.Conta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("conta")
public class ContaController {

    @Autowired
    private ContaDao dao;

    @GetMapping("form")
    public String form() {
        return "/conta/form";
    }

    @PostMapping("adiciona")
    public String adicionar(Conta conta) {
        dao.adiciona(conta);
        return "redirect:/conta/form";
    }
}
